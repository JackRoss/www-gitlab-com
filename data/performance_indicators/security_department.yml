- name: Security Hiring Actual vs Plan
  base_path: "/handbook/engineering/security/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-hiring-actual-vs-plan"
  definition: Employees are in the division "Engineering" and department is "Security".
  target: 0.90
  org: Security Department
  health:
    level: 3
    reasons:
    - 'Health: Monitor health closely'
  sisense_data:
    chart: 8636794
    dashboard: 592612
    embed: v2
  urls:
  - "/handbook/hiring/charts/security-department/"

- name: Security Non-Headcount Plan vs Actuals
  base_path: "/handbook/engineering/security/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-non-headcount-budget-vs-plan"
  definition: We need to spend our investors' money wisely. We also need to run a
    responsible business to be successful, and to one day go on the public market.
  target: Unknown until FY21 planning process
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - Currently finance tells me when there is a problem, I’m not self-service.
    - Get the budget captured in a system
    - Chart budget vs. actual over time in periscope
  urls:
  - https://app.periscopedata.com/app/gitlab/633239/Security-Non-Headcount-BvAs

- name: Security Average Location Factor
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor by function and department so managers can make tradeoffs
    and hire in an expensive region when they really need specific talent unavailable
    elsewhere, and offset it with great people who happen to be in low cost areas.
    Data comes from BambooHR and is the average location factor of all team members
    in the Security department.
  target: Less than 0.66
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - Security Operations having challenges pulling quality candidates in geo-diverse
      locations, in order to hit expectations and allow the team to grow, we will
      revisit our backlog US-based candidates.
  sisense_data:
    chart: 7864119
    dashboard: 592612
    embed: v2
  urls:
  - "/handbook/hiring/charts/security-department/"

- name: Security Overall Handbook Update Frequency Rate
  base_path: "/handbook/engineering/security/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-update-frequency"
  definition: The handbook is essential to working remote successfully, to keeping
    up our transparency, and to recruiting successfully. Our processes are constantly
    evolving and we need a way to make sure the handbook is being updated at a regular
    cadence. This data is retrieved by querying the API with a python script for merge
    requests that have files matching `/source/handbook/engineering/security` over
    time.
  target: 1
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - Increase in PTO, decrease in engagement.
  sisense_data:
    chart: 10642322
    dashboard: 621064
    shared_dashboard: feac7198-86db-480b-9eae-c41cb479a209
    embed: v2

- name: Security Department Narrow MR Rate
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: This PI is in support of the engineering organization’s overall MR Rate objective however, this should not be considered a reflection of the performance or output of the Security Department whose work is primarily handbook driven.  Thus, there is no current target for Security Department MR Rate.
  target: 0
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/4446
  sisense_data:
    chart: 8934521
    dashboard: 686930
    shared_dashboard: ec910110-91bd-4a08-84aa-223bf6b3c772
    embed: v2

- name: MTTR (Mean-Time-To-Remediation) for severity 1, 2, and 3 security vulnerabilities
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: The MTTR metric is an indicator of our efficiency in remediating security
    vulnerabilities, whether they are reported through HackerOne bug bounty program
    (or other external means, such as security@gitlab.com emails) or internally-reported.
    The average days to close issues in the GitLab project (project_id = '278964')
    that are have the label `security` and severity 1, severity 2, or severity 3; this excludes issues with
    variation of the security label (e.g. `security review`) and severity 4 issues. Issues
    that are not yet closed are excluded from this analysis. This means historical
    data can change as older issues are closed and are introduced to the analysis.
    The average age in days threshold is set at the daily level.
  target: https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues
  org: Security Department
  is_key: true
  health:
    level: 1
    reasons:
    - MTTR metrics for severity 2 and 3 vulnerabilities are showing they need attention due to them being outside of our current SLOs listed in the <a href="https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues">Security Handbook</a>. Severity 2 vulnerabilities have been missing the target SLO since November 2018 and severity 3 vulnerabilities missing since September 2019.    
  urls:
  - https://app.periscopedata.com/app/gitlab/641782/WIP:-Appsec-hackerone-vulnerability-metrics?widget=8729826&udv=0
  - https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues

- name: Average age of currently open bug bounty vulnerabilities by severity
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: The average age of currently open bug bounty vulnerabilities gives a
    health snapshot of how fast we are fixing the vulnerabilities that currently exist.
    This is important because it can help indicate what our future MTTR will look
    like and whether we are meeting our defined SLAs. The query is built by using
    the `ultimate_parent_id` of `9970` and is only for `open` `state` issues labelled
    with `security` and `hackerone`. The average age is measured in days, and the
    targets for each severity are defined in https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues.
  target: https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - Average age of currently open bug bounty vulnerabilities by severity gives a
      present health snapshot of our ability to fix vulnerabilites.
  urls:
  - https://app.periscopedata.com/app/gitlab/641782/WIP:-Appsec-hackerone-vulnerability-metrics?widget=8715519&udv=0

- name: Net vulnerability count for each month
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: The net vulnerability count for each month allows us to see whether
    we are adding or eliminating from the security backlog. This is the open count
    - close count for each month. It's important to note that these are only for bug
    bounty reported vulnerabilites. This chart is intended to be used with other data
    to try and illustrate a more detailed story.
  target: At or below 0
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - The security vulnerability backlog has only been slightly increasing over the
      past few months.
  urls:
  - https://app.periscopedata.com/app/gitlab/641782/Appsec-hackerone-vulnerability-metrics?widget=8421209&udv=0

- name: HackerOne budget vs actual (with forecast of 'unknowns')
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: We currently run a public bug bounty program through HackerOne, and
    this program has been largely successful - we get a lot of hacker engagement,
    and since the program went public, we have been able to resolve nearly 100 reported
    security vulnerabilities. The bounty spend is however, a budgeting forecast concern
    because of the unpredictability factor from month to month.
  target: TBD
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - H1 spend has been decreasing, even after the bump to "critical" and "high" findings.
      Projected spend for Q1 20 is $80k, compared to Q1 19's $180k.

- name: Security Engineer On-Call Page Volume
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: This metric is focused around the volume and severity of paged incidents
    to the Security Engineer On-Call.
  target: Number of pages/month does not exceed +50% of monthly average of the last 12 months for 3 consecutive months
  org: Security Department
  is_key: true
  health:
    level: 3
    reasons:
    -
  urls:
  - https://app.periscopedata.com/app/gitlab/592612/Security-KPIs?widget=9217413&udv=0

- name: Security Control Health
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: GCF security controls are conitnuously tested as parts of the Compliance
    team's continuous monitoring program, internal audits and external audits. A clear
    indicator of success is directly reflected in the control effectveness rating.
    Observations are a result of GCF security failure, indicating that the control
    is not implemented, designed effectively or is not operating effectively. These
    observations indicate a gap that requires remediation in order for the security
    control to be operating and audit ready.
  target: This will be determined in FY22 Q1 as part of GRC application onboarding
  org: Security Department
  is_key: true
  health:
    level: 2
    reasons:
    - The 2020 SOC 2 Type 2 and SOC 3 Type 2 examination was successfully executed in November and December 2020.
  urls:
  - https://docs.google.com/spreadsheets/d/157htqF6TB_6vjt47zP9n2GN3lPGVPRVPPcnLu5ayPYk/edit?usp=sharing

- name: Security Impact on ARR
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: The Field Security organization functions as a sales and customer enablement
    team therefore a clear indicator of success is directly reflected in the engagement
    of their services by Legal, Sales, TAMs and customers themselves.
  target: 60%
  org: Security Department
  is_key: true
  health:
    level: 3
    reasons:
    - Due to standard end of year renewals, Field Security observed a marked uptick in sales closures that did not require Field Security Support. We are currently working with the data team to identify a way to remove renewals from the data set. 
  urls:
    - https://app.periscopedata.com/app/gitlab/775136/WIP--Risk-and-Field-Security-KPIs?widget=10484456&udv=0
  sisense_data:
    dashboard: 775136
    chart: 10484456
    embed: v2 

- name: Cost of Abuse
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: This metric is focused around the financial impact of abusive accounts and their activity.
  target: Cost of abuse is below $10K/Mo
  org: Security Department
  is_key: true
  health:
    level: 2
    reasons:
    - The data currently presented is based off a small (~1000) number of abusive accounts that were auto-detected because their activity was highly anomalous. We have tagged all user accounts that has been blocked by the Trust and Safety team using [Custom Attributes](https://docs.gitlab.com/ee/api/custom_attributes.html) and it's been added as a [new data source](https://gitlab.com/gitlab-data/analytics/-/issues/7411) which is available via Sisense. We are in the process of creating the first dashboards. ETA end of FY21-Q4
  urls:
  - https://app.periscopedata.com/app/gitlab/761157/Cost-of-Abuse
  - https://app.periscopedata.com/app/gitlab/780726/WIP:-Blocked-User-Usage-Data

- name: Security Discretionary Bonus Rate
  base_path: "/handbook/engineering/security/performance-indicators/index.html#security-discretionary-bonuses"
  definition: Discretionary bonuses offer a highly motivating way to reward individual
    GitLab team members who really shine as they live our values. Our goal is to award
    discretionary bonuses to 10% of GitLab team members in the Security department
    every month.
  target: At or above 10%
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - We currently track bonus percentages in aggregate, but there is no easy way
      to see the percentage for each individual department.
      
- name: Security Department New Hire Average Location Factor
  base_path: "/handbook/engineering/security/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-division-new-hire-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor for team members hired within the past 3 months so
    hiring managers can make tradeoffs and hire in an expensive region when they really
    need specific talent unavailable elsewhere, and offset it with great people who
    happen to be in more efficient location factor areas with another hire. The historical average location factor represents the average location factor for only new hires in the last three months, excluding internal hires and promotions. The calculation for the three-month rolling average location factor is the location factor of all new hires in the last three months divided by the number of new hires in the last three months for a given hire month. The data source is BambooHR data.
  target: Less than 0.66
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - We've been fluctuating  above and below the target recently, which is to be expected
      given the size of the department.
  sisense_data:
    chart: 9389232
    dashboard: 719541
    embed: v2
