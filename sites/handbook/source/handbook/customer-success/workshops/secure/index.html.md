---
layout: handbook-page-toc
title: "Secure Workshop"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

⛔️⚠️ **This workshop is still under development and is not yet ready for customer delivery. This page, and the [OKR for the workhop's development](https://gitlab.com/gitlab-com/customer-success/okrs/-/issues/128), will be updated as it progresses.** ⚠️⛔️

## Overview

The Secure Workshop is a Customer Success activity to provide customer enablement on [Secure stage features](https://about.gitlab.com/stages-devops-lifecycle/secure/).

The objectives of a Secure Workshop are to:

- Provide the customer with best practices for Secure capabilities
- Help the customer to adopt Secure stage features they already have access to
- Show the customer the value of the Secure stage to position tier upgrade

A Secure Workshop is **not** a hands-on-keyboard demo or implementation effort.

## Getting Started

The first step in setting up a Secure workshop with a customer is working through the [discovery questionnaire](https://docs.google.com/document/d/1bQwVFU1UInjdIg5oSoNHXT8O1PdupKXCIft0yeAhgE8/edit?usp=sharing). This will help you to scope the workshop, and plan what information to cover.

_This enablement is still under development, and the customer-facing materials are not yet available. This page will be updated as it progresses._
