---
layout: handbook-page-toc
title: "Social Media Reporting"
description: Metric Definitions, Cadences, and Objectives
twitter_image: "/images/opengraph/handbook/social-marketing/social-handbook-reporting.png"
twitter_image_alt: "GitLab's Social Media Handbook branded image"
twitter_site: "gitlab"
twitter_creator: "gitlab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Social Media Reporting
Reporting on our brand's organic social media efforts is critical in determining if what we're doing is working and understanding our audiences better. This page outline specifics on our reporting, including definitions of metrics.

## Dashboards and Reports 
For GitLab Team Members only

| I'm looking for...                           | ...this report or dashboard will help.                                                    |
|----------------------------------------------|----------------------------------------------------------------------------------------|
| Overall Brand Social Performance             | [Sisense Brand Social Performance Dashboard ](https://app.periscopedata.com/app/gitlab/621921/Organic-Social-Media-Metrics-(for-GitLab-Brand-Channels))                                            |
| Sheet to enter raw performance data          | [Raw metrics sheetload for Sisense](https://docs.google.com/spreadsheets/d/1Lc3uLs7gpoYYu10cLlzqzxWCFISja1Df-FSvY_xBQU4/edit#gid=0) (available to social and data team only) |
| Ad equivalency dashboard for all social data | [Social Ad Equivalency Dashboard by objective and campaign](https://docs.google.com/spreadsheets/d/1sZwoUwnk5BXHrmRkAPkipgtMJZ8_stS-hMagTujym0k/edit?usp=sharing)                              |

## Where does our data come from? 
Our brand social team uses Sprout Social as our social media management software to schedule, engage, and report social media efforts. The majority of this data is already available natively across channels but is not curated or collated well for our purposes. **Our single source of truth for all organic brand social data is Sprout Social, however, for our wider team, Sisense acts as our data warehouse and visualizer.**

[Learn how we pull data from Sprout, add it to a sheetload, and get it into Sisense with this video](https://youtu.be/gol6eKcmIew?t=83) (available to team members logged into our GitLab Unfiltered YouTube account only).

## Social Media Metric Definitions

**Impressions** are the number of times that our content was displayed to users. There are a few caveats:
- Twitter: This includes when other users retweet our posts.
- LinkedIn: This does not include when other users share our posts.
- Facebook: the # of times that any content associated with our page, not just a post (think updating images), was displayed to a user.
- Instagram: This does not include profile visitors.
- We use impressions = views, as defined for Corporate Marketing. This is not our most important metric for social-only performance but is one of the few metrics directly attributable to our shared top-of-funnel activity across teams.

**Engagements** are the number of times that users took action on our posts during the reporting period. This includes likes, retweets, comments, saves, replies, reactions, and shares. Does not include quote retweets.

**Post Link Clicks** (Link clicks or clicks for short) is the number of times that users clicked on links from your posts during the reporting period

**Ad Equivalency** is the GitLab-specific social media advertising dollar equivalent organized by post objective
- impressions (measured by CPM)
- link clicks (measured by CPC)
- Ad Equivalency is a value-driven metric that is entirely determined by our real-world social advertising spend. We'll evaluate the ad equivalency per channel/quarter average. 

**Net Follower Growth** is the total number of new followers, less the amount of unfollows. 

**Follower Growth Rate** is a percentage of growth over a reporting period. While we may look at this month-over-month, it's best-reviewed quarter-over-quarter or year-over-year.

**Engagement Rate** is the number of times during a reporting period that users engaged with our posts as a percentage of impressions. This metric indicates how engaging particular content or campaigns are overall.

## Reporting Cadences 

Our cadences allow us regular reporting periods and the ability to be accountable to our team members in other groups or report performance regularly. The social team will not accommodate random reporting requests that were not previously required, agreed upon, or that the team feels inappropriate to use time or review the data.

### Monthly Reporting 

The social team will pull data for overall brand performance and select topics/campaigns before the end of the second week of the month following the reporting month (e.g., January data will be available before mid-February). [The Sisense dashboard](https://app.periscopedata.com/app/gitlab/621921/Organic-Social-Media-Metrics-(for-GitLab-Brand-Channels)) takes approximately 24-hours to refresh after raw data is added to our sheetload.

We will also add the necessary data to our [Ad Equivalency dashboard](https://docs.google.com/spreadsheets/d/1sZwoUwnk5BXHrmRkAPkipgtMJZ8_stS-hMagTujym0k/edit?usp=sharing) in Google Sheets. 

### Quarterly Reporting

We'll be able to review the full quarter of [overall brand performance in Sisense](https://app.periscopedata.com/app/gitlab/621921/Organic-Social-Media-Metrics-(for-GitLab-Brand-Channels)) following the refresh with the third month of data added, typically before the end of the second week of the new quarter.

When the social team adds the third month of data from a quarter to the [ad equivalency dashboard](https://docs.google.com/spreadsheets/d/1sZwoUwnk5BXHrmRkAPkipgtMJZ8_stS-hMagTujym0k/edit?usp=sharing), we'll include the average CPC and CPM by social channel, closing the metrics for the quarter.

Quarterly data makes its way into various reports shared across the company. 

### Annual Reporting

This is TBD as we build our first annual report.


